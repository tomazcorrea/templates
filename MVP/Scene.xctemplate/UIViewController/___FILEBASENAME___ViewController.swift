//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

class ___VARIABLE_sceneName___ViewController: UIViewController {
    
    // MARK: - OUTLETS -
    
    // MARK: - CONSTANTS -
    
    // MARK: - VARIABLES -
    private var presenter: ___VARIABLE_sceneName___Presenter!
    private lazy var viewData = ___VARIABLE_sceneName___ViewData()
    
    // MARK: - IBACTIONS -
}

// MARK: - LIFE CYCLE -
extension ___VARIABLE_sceneName___ViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = ___VARIABLE_sceneName___Presenter(viewDelegate: self)
    }
}

// MARK: - PRESENTER -
extension ___VARIABLE_sceneName___ViewController: ___VARIABLE_sceneName___ViewDelegate {

}

// MARK: - AUX METHODS -
extension ___VARIABLE_sceneName___ViewController {

}
