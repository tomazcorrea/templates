//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

// MARK: - STRUCT VIEW DATA -
struct ___VARIABLE_sceneName___ViewData {
    
}

// MARK: - VIEW DELEGATE -
protocol ___VARIABLE_sceneName___ViewDelegate: NSObjectProtocol {
    
}

// MARK: - PRESENTER CLASS -
class ___VARIABLE_sceneName___Presenter {
    
    private weak var viewDelegate: ___VARIABLE_sceneName___ViewDelegate?
    private var viewData = ___VARIABLE_sceneName___ViewData()
    
    init(viewDelegate: ___VARIABLE_sceneName___ViewDelegate) {
        self.viewDelegate = viewDelegate
    }
}

// MARK: - SERVICE -
extension ___VARIABLE_sceneName___Presenter {
    
}

// MARK: - AUX METHODS -
extension ___VARIABLE_sceneName___Presenter {
    
}

// MARK: - DATABASE -
extension ___VARIABLE_sceneName___Presenter {
    
}
